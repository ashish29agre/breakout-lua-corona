-----------------------------------------------------------------------------------------
--
-- level1.lua
--
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local buttons = require ("modules.buttons")
local scene = composer.newScene()

-- include Corona's "physics" library
local physics = require "physics"
physics.start(); physics.pause()
--physics.setDrawMode("hybrid")

local startText
local breakOutObjects = {}
local ball
local paddle
local boundaryTop
local boundaryLeft
local boundaryRight

--------------------------------------------

-- forward declarations and other locals
local screenW, screenH, halfW = display.contentWidth, display.contentHeight, display.contentWidth*0.5

function scene:create( event )

	-- Called when the scene's view does not exist.
	-- 
	-- INSERT code here to initialize the scene
	-- e.g. add display objects to 'sceneGroup', add touch listeners, etc.

	local sceneGroup = self.view

	startText = display.newText("Start", halfW, screenW, native.systemFont, 24)
  sceneGroup:insert(startText)

  -- add bricks
  spawnBricks(sceneGroup)
	 --add paddle
   print("Paddle y " .. screenH-48)
  paddle = display.newRoundedRect(halfW, screenH-48, 72, 16, 8, 8)
--  paddle = display.newImage("buttons/btn_teal.png", halfW, screenH)
  paddle.name = "paddle"
  physics.addBody(paddle, "static")
  paddle.strokeWidth = 3
  paddle:setFillColor( 0.5 )
  paddle:setStrokeColor( 1, 1, 1 )
  paddle:addEventListener("touch", paddleMover)
  sceneGroup:insert(paddle)
  
  --create boundary
  createBoundaryAndAttachListeners(sceneGroup)
  createBallAndAttachListener(sceneGroup)
  
  
end

function spawnBricks(sceneGroup)
  local yPos, xPos = 72, 82
  for i=1, 12, 1
    do
        --load buttons string from modules
        local btn = buttons.getButton(math.random(1, 6))
        --print("Button path " .. btn)
        local btnObject = display.newImage(btn, xPos, yPos)
        btnObject.name = "brick"
        physics.addBody(btnObject, "static")
        --print("Btn object position " .. btnObject.x)
        if(i % 3 == 0)
          then 
            xPos = 0
            yPos = yPos + btnObject.contentHeight + 10
            --print("Increased y position to " .. yPos ..  " xpos is " .. xPos)
        end
        
        
        xPos = xPos + btnObject.contentWidth + 10
        sceneGroup:insert(btnObject)
        table.insert(breakOutObjects, btnObject)
        
       
        
  end    
end


function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then
		-- Called when the scene is still off screen and is about to move on screen
	elseif phase == "did" then
		-- Called when the scene is now on screen
		-- 
		-- INSERT code here to make the scene come alive
		-- e.g. start timers, begin animation, play audio, etc.
		physics.start()
	end
end

function scene:hide( event )
	local sceneGroup = self.view
	
	local phase = event.phase
	
	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
		physics.stop()
	elseif phase == "did" then
		-- Called when the scene is now off screen
	end	
	
end

function scene:destroy( event )

	-- Called prior to the removal of scene's "view" (sceneGroup)
	-- 
	-- INSERT code here to cleanup the scene
	-- e.g. remove display objects, remove touch listeners, save state, etc.
	local sceneGroup = self.view
	
	package.loaded[physics] = nil
	physics = nil
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

ballCollision = function(self, event)
  print("Event ", event.other.name)
end  

ballEnterFrame = function(self, event)
  --print("Basll yPos " .. self.y)
  --print("Basll yPos " .. self.y)
   if ( ball.y > screenH ) then
      print("Reset ball position")
      ball.x = halfW
      ball.y = screenH-72
      ball:setLinearVelocity(0,0)
      applyForceToBall()
   end
end  

paddleMover = function(event)


      if(event.phase == "moved")
        then
          local xPaddlePos = event.x
          if(xPaddlePos< event.target.contentWidth * 0.5 ) 
            then
              xPaddlePos= event.target.contentWidth * 0.5
          elseif(xPaddlePos >  screenW -(event.target.contentWidth * 0.5))
            then
              xPaddlePos = screenW - event.target.contentWidth * 0.5
          end
          event.target.x = xPaddlePos
          return true
      end    

end 

function applyForceToBall()
  ball:applyForce( 1, 1, ball.x, ball.y)
  --ball:setLinearVelocity( 75, 150 )
end  

function createBallAndAttachListener(sceneGroup)
  --print("ball y " .. screenH)
  ball = display.newImage("ball.png", halfW, paddle.y-paddle.contentHeight)
 
  local scaleX, scaleY =  0.5, 0.5 
  --ball:scale( scaleX, scaleY )
  local nw, nh = ball.width*scaleX*0.5, ball.height*scaleY*0.5;

  physics.addBody( ball, "dynamic", { friction=0, bounce=1 });
  ball.angularVelocity = 0
  ball.isFixedRotation = true
  sceneGroup:insert(ball)
  applyForceToBall()
  ball.collision = ballCollision
  ball.enterFrame = ballEnterFrame
  ball:addEventListener("collision")
  Runtime:addEventListener("enterFrame", ball)
end  


function createBoundaryAndAttachListeners(sceneGroup)
  print("Creating boundary")
  boundaryTop = display.newRoundedRect(0, 0, screenW*2, 1, 0)
  boundaryTop.strokeWidth = 1
  boundaryTop:setFillColor( 0.5 )
  boundaryTop:setStrokeColor( 1, 1, 1 )
  sceneGroup:insert(boundaryTop)
  
  boundaryLeft = display.newRoundedRect(0, 0, 1, screenH*2, 0)
  boundaryLeft.strokeWidth = 1
  boundaryLeft:setFillColor( 0.5 )
  boundaryLeft:setStrokeColor( 1, 1, 1 )
  sceneGroup:insert(boundaryLeft)
  
  boundaryRight = display.newRoundedRect(screenW, 0, 1, screenH*2, 0)
  boundaryRight.strokeWidth = 1
  boundaryRight:setFillColor( 0.5 )
  boundaryRight:setStrokeColor( 1, 1, 1 )
  sceneGroup:insert(boundaryRight)
end
  

-----------------------------------------------------------------------------------------

return scene