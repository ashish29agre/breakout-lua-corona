local buttons = {}
print("Modules buttons loaded")

--table of buttons strings
local buttonStrings = {}

table.insert(buttonStrings,"buttons/btn_blue.png")
table.insert(buttonStrings,"buttons/btn_dark_blue.png")
table.insert(buttonStrings,"buttons/btn_green.png")
table.insert(buttonStrings,"buttons/btn_orange.png")
table.insert(buttonStrings,"buttons/btn_teal.png")
table.insert(buttonStrings,"buttons/btn_yellow.png")

function buttons.getButton(index)
  if(index == 1) then return buttonStrings[1]
  elseif(index == 2) then return  buttonStrings[2]
  elseif(index == 3) then return  buttonStrings[3]
  elseif(index == 4) then return  buttonStrings[4]
  elseif(index == 5) then return  buttonStrings[5]
  elseif(index == 6) then return  buttonStrings[6]
  end  
end  

return buttons